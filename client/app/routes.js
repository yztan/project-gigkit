var app = angular.module('appRoutes', ['ngRoute','angular-filepicker'])

.config(function($routeProvider,$locationProvider,filepickerProvider){
   
    $routeProvider

    .when('/', {
        templateUrl: 'app/views/home.html',
        controller: 'HomeController',
    })

    .when('/about', {
        templateUrl: 'app/views/about.html'
    })

    .when('/register', {
        templateUrl: 'app/views/users/register.html',
        controller: 'regCtrl',
        controllerAs: 'register',
        authenticated: false
    })

    .when('/login', {
        templateUrl: 'app/views/users/login.html',
        authenticated: false

    })

    .when('/logout', {
        templateUrl: 'app/views/users/logout.html',
        authenticated: true
    })

    .when('/profile', {
        templateUrl: 'app/views/users/profile.html',
        
        authenticated: true
    })
    
    .when('/facebook/:token', {
        templateUrl: 'app/views/users/social/social.html',
        controller:'facebookCtrl',
        controllerAs: 'facebook',
        authenticated: false
    })

    .when('/facebookerror', {
        templateUrl: 'app/views/users/login.html',
        controller:'facebookCtrl',
        controllerAs: 'facebook',
        authenticated: false
    })
    .when('/gigs', {
        templateUrl: 'app/views/gigs.html',
        controller: 'GigsController',
        authenticated: true
        
    })
    .when('/gallery', {
        templateUrl: 'app/views/gallery.html',
        controller: 'galleryController',
        authenticated: true
        
    })
    .when('/management', {
        templateUrl: 'app/views/management/management.html',
        controller: 'managementCtrl',
        controllerAs: 'management',
        authenticated: true,
        permission: ['admin','moderator']
    })
    .when('/edit/:id', {
        templateUrl: 'app/views/management/edit.html',
        controller: 'editCtrl',
        controllerAs: 'edit',
        authenticated: true,
        permission: ['admin','moderator']
    })

    .when('/addArtist', {          
        templateUrl: 'app/views/addArtist.html',
        controller: 'addSuperheroController',
        authenticated: true
    })   
    .when('/addGig', {          
        templateUrl: 'app/views/addGig.html',
        controller: 'addGigController'       ,
        authenticated: true,
           
    })   
    .when('/book/:id', {
        templateUrl: 'app/views/book.html',
        controller: 'bookController',
        authenticated: true
    })   
    .when('/detail/:id', {
        templateUrl: 'app/views/detail.html',
        controller: 'detailController',
        authenticated: true,
     
    })
    .when('/delete/:id', {
        templateUrl: 'app/views/delete.html',
        controller: 'deleteController',
        authenticated: true,
      
    })

    .when('/deleteGig/:id', {
        templateUrl: 'app/views/deleteGig.html',
        controller: 'deleteGigController',
        authenticated: true,
        
    })

    
    .when('/map', {
        templateUrl: 'app/views/map.html',
        authenticated: true,
        permission: ['admin','moderator','user']
    })


    .otherwise({redirectTo: '/'});

    filepickerProvider.setKey('AynkfxksOQNSa83fviAQKz');

    $locationProvider.html5Mode({
        enabled:true,
        requiredBase: false
    });
});

app.run(['$rootScope', 'Auth', '$location','User',  function($rootScope, Auth, $location, User){

    $rootScope.$on('$routeChangeStart', function(event, next, current) {

        if(next.$$route.authenticated == true) {
            if(!Auth.isLoggedIn()){
                event.preventDefault();
                $location.path('/');
            } else if (next.$$route.permission){
                User.getPermission().then(function(data) {
                    // Check if user's permission matches at least one in the array
                    if (next.$$route.permission[0] !== data.data.permission) {
                        if (next.$$route.permission[1] !== data.data.permission) {
                            event.preventDefault(); // If at least one role does not match, prevent accessing route
                            $location.path('/'); // Redirect to home instead
                        }
                    }
                });
            }
        } else if (next.$$route.authenticated == false) {
            if(Auth.isLoggedIn()) {
                event.preventDefault();
                $location.path('/profile')
            }
      
        } else {
           
        }

        console.log(next.$$route.authenticated);
    })
}]);


