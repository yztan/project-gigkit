angular.module('deleteGigCtrl', [])
.controller('deleteGigController', function($scope, $http, $routeParams){
    $scope.gig = {};
    //get the id to query the db and retrieve the correct superhero
    var id = $routeParams.id;
    // $http.get('/superhero/' + id)
    //     .then(function(data){
    //         console.log(JSON.stringify(data));
    //         $scope.superhero = data;
    //     },
    //     function(data) {
    //         console.log('Error: ' + data);
    //     });     

    $http.delete('/api/gig/delete/'+ id)
    .then(function(data){
        console.log(JSON.stringify(data));
        $scope.gig = data;
    },
    function(data) {
        console.log('Error: ' + data);
    });     
    
});