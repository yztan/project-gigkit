angular.module('addGigCtrl', [])
.controller('addGigController', function($scope, $http,$location,$timeout){
    $scope.gig = {};
    //Send the newly created superhero to the server to store in the db
    $scope.createGig = function(){
        $http.post('/api/gig', $scope.gig)
            .then(function(data){
                console.log(JSON.stringify(data));
              
                //Clean the form to allow the user to create new superheroes
                $scope.gig = {};
                $timeout(function(){
                    $location.path('/gigs');
                    },2000);
            },
            function(data) {
                console.log('Error: ' + data);
            });
        }; 
  

 
});