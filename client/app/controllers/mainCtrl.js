angular.module('mainController',['authServices'])

.controller('mainCtrl',function(Auth,$timeout,$location,$rootScope, $window, $route, User){
   
    var app = this;

    app.loadme = false;

    var showModal = function(option){
        app.choiceMade = false;
        app.modalHeader = undefined;
        app.modalBody = undefined;
        app.hideButton = false;

        if(option === 1 ){
            app.modalHeader = 'Timeout Warning';
            app.modalBody = 'Yoursession will expire in 5 minutes. Would you like to renew your session?'
            $('#myModal').modal({backdrop:"static"});
        } else if (option === 2) {
            app.hideButton = true;
            app.modalHeader = 'Logging out';
            $('#myModal').modal({backdrop:"static"});
            $timeout(function() {
                Auth.logout();
                $location.path("/");
                hideModal();
                $route.reload();
            }, 3000);
        }
        $timeout(function(){
            if(!app.choiceMade){
                console.log("Logged out!");
                hideModal();
            }
        }, 5000);
    };

    var hideModal = function() {
        $('#myModal').modal('hide');
    };

        // Check if user is on the home page
        $rootScope.$on('$routeChangeSuccess', function() {
            if ($window.location.pathname === '/') {
                app.home = true; // Set home page div
            } else {
                app.home = false; // Clear home page div
            }
        });

    $rootScope.$on('$routeChangeStart',function(){
        if(Auth.isLoggedIn()){
            console.log("Success: User is logged in");
            app.isLoggedIn = true;
            Auth.getUser().then(function(data){
                console.log(data.data.username)
                app.username = data.data.username;
                app.useremail = data.data.email;
              
                User.getPermission().then(function(data){
                    if(data.data.permission === 'admin' || data.data.permission === ' moderator'){
                        app.authorized = true;
                        app.loadme = true;    
                    } else {
                        app.authorized = false;
                        app.loadme = true;
                    }
                })

            })
        } else {
            console.log("Failure: User is NOT logged in.")
            app.isLoggedIn = false;
            app.username = '';
            app.loadme = true;
        }
        if ($location.hash() == '_=_') $location.hash(null);
            app.disabled = false;
            app.loadme = true;
    });

    this.facebook = function(){
        $window.location = $window.location.protocol + '//' + $window.location.host + '/auth/facebook';

    }

    this.doLogin = function(loginData){
        app.loading = true;
        app.errorMsg = false;

               Auth.login(app.loginData).then(function(data){
                        if(data.data.success){
                            app.loading = false;
                       //Create Success Message
                        app.successMsg = data.data.message + '....Redirecting';
                        //redirect
                        $timeout(function(){
                        $location.path('/home');
                        app.loginData = '';
                        app.successMsg = false;
                        },2000);
                   }else {
                       app.loading = false;
                       app.errorMsg = data.data.message;
                   }
               });
        };

        this.logout = function(){
            showModal(2);
        };
    });

