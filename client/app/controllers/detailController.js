angular.module('detailCtrl', [])
.controller('detailController', function($scope, $http, $routeParams){
    $scope.superhero = {};
    //get the id to query the db and retrieve the correct superhero
    var id = $routeParams.id;
    $http.get('/api/superhero/' + id)
        .then(function(data){
            console.log(JSON.stringify(data));
            $scope.superhero = data;
        },
        function(data) {
            console.log('Error: ' + data);
        });     

    // $scope.deleteSuperhero = $http.delete('/superhero/'+ id)
    // .then(function(data){
    //     console.log(JSON.stringify(data));
    //     $scope.superhero = data;
    // },
    // function(data) {
    //     console.log('Error: ' + data);
    // });     
    
});