angular.module('managementController', [])
.controller('managementCtrl', function(User){
    var app = this;
    
    app.loading = true;
    app.accessDenied = true;
    app.errorMsg = false;
    app.editAccess = false;
    app.deleteAccess = false;
    app.limit = 5;

    
      // Function: get all the users from database
      function getUsers() {
        // Runs function to get all the users from database
        User.getUsers().then(function(data) {
            // Check if able to get data from database
            if (data.data.success) {
                // Check which permissions the logged in user has
                if (data.data.permission === 'admin' || data.data.permission === 'moderator') {
                    app.users = data.data.users; // Assign users from database to variable
                    app.loading = false; // Stop loading icon
                    app.accessDenied = false; // Show table
                    // Check if logged in user is an admin or moderator
                    if (data.data.permission === 'admin') {
                        app.editAccess = true; // Show edit button
                        app.deleteAccess = true; // Show delete button
                    } else if (data.data.permission === 'moderator') {
                        app.editAccess = true; // Show edit button
                    }
                } else {
                    app.errorMsg = 'Insufficient Permissions'; // Reject edit and delete options
                    app.loading = false; // Stop loading icon
                }
            } else {
                app.errorMsg = data.data.message; // Set error message
                app.loading = false; // Stop loading icon
            }
        });
    }

    getUsers(); // Invoke function to get users from databases
    
        // Function: Show more results on page
        app.showMore = function(number) {
            app.showMoreError = false; // Clear error message
            // Run functio only if a valid number above zero
            if (number > 0) {
                app.limit = number; // Change ng-repeat filter to number requested by user
            } else {
                app.showMoreError = 'Please enter a valid number'; // Return error if number not valid
            }
        };
    
        // Function: Show all results on page
        app.showAll = function() {
            app.limit = undefined; // Clear ng-repeat limit
            app.showMoreError = false; // Clear error message
        };


    // Function: Delete a user
    app.deleteUser = function(username) {
        // Run function to delete a user
        User.deleteUser(username).then(function(data) {
            // Check if able to delete user
            if (data.data.success) {
                getUsers(); // Reset users on page
            } else {
                app.showMoreError = data.data.message; // Set error message
            }
        });
    };
})
.controller('editCtrl', function($scope, $routeParams, User, $timeout) {
    var app = this;
    $scope.nameTab = 'active'; // Set the 'name' tab to the default active tab
    app.phase1 = true; // Set the 'name' tab to default view

    User.getUser($routeParams.id).then(function(data) {
        // Check if the user's _id was found in database
        if (data.data.success) {
            $scope.newName = data.data.user.name;
            $scope.newEmail = data.data.user.email;
            app.currentUser = data.data.user._id;
            $scope.newUsername = data.data.user.username; // Display user's username in scope
            $scope.newPermission = data.data.user.permission; // Display user's permission in scope
        } else {
            app.errorMsg = data.data.message;
        }
    });

    app.namePhase = function() {
        $scope.nameTab = 'active';
        $scope.usernameTab = 'default';
        $scope.emailTab = 'default';
        $scope.permissionTab = 'default';
        app.phase1 = true;
        app.phase2 = false;
        app.phase3 = false;
        app.phase4 = false;
        app.errorMsg = false; // Clear error message
    };
    app.emailPhase = function() {
        $scope.nameTab = 'default';
        $scope.usernameTab = 'default';
        $scope.emailTab = 'active';
        $scope.permissionTab = 'default';
        app.phase1 = false;
        app.phase2 = false;
        app.phase3 = true;
        app.phase4 = false;
        app.errorMsg = false; // Clear error message
    };
    app.usernamePhase = function() {
        $scope.nameTab = 'default';
        $scope.usernameTab = 'active';
        $scope.emailTab = 'default';
        $scope.permissionTab = 'default';
        app.phase1 = false;
        app.phase2 = true;
        app.phase3 = false;
        app.phase4 = false;
        app.errorMsg = false; // Clear error message
    }
    app.permissionsPhase = function() {
        $scope.nameTab = 'default';
        $scope.usernameTab = 'default';
        $scope.emailTab = 'default';
        $scope.permissionTab = 'active';
        app.phase1 = false;
        app.phase2 = false;
        app.phase3 = false;
        app.phase4 = true;
        app.disableUser = false; // Disable buttons while processing
        app.disableModerator = false; // Disable buttons while processing
        app.disableAdmin = false; // Disable buttons while processing
        app.errorMsg = false; // Clear any error messages
        // Check which permission was set and disable that button
        if ($scope.newPermission === 'user') {
            app.disableUser = true; // Disable 'user' button
        } else if ($scope.newPermission === 'moderator') {
            app.disableModerator = true; // Disable 'moderator' button
        } else if ($scope.newPermission === 'admin') {
            app.disableAdmin = true; // Disable 'admin' button
        }
    };


    app.updateName = function(newName, valid) {

        app.errorMsg = false;
        app.disabled = true;
        

        if(valid) {
            var userObject = {};
            userObject._id = app.currentUser;
            userObject.name = $scope.newName;
            User.editUser(userObject).then(function(data){
                if(data.data.success){
                    app.successMsg = data.data.message;
                    $timeout(function() {
                        app.nameForm.name.$setPristine();
                        app.nameForm.name.$setUntouched();
                        app.successMsg = false;
                        app.disabled = false;
                    }, 2000)
                } else {
                    $scope.alert = 'alert alert-danger'; // Set class for message
                    app.errorMsg = data.data.message; // Clear any error messages
                    app.disabled = false; // Enable form for editing
                }
            });
        } else {
            $scope.alert = 'alert alert-danger'; // Set class for message
            app.errorMsg = 'Please ensure form is filled out properly'; // Set error message
            app.disabled = false; // Enable form for editing
        }
    };

    app.updateEmail = function(newEmail, valid) {
        
                app.errorMsg = false;
                app.disabled = true;
      
        
                if(valid) {
                    var userObject = {};
                    userObject._id = app.currentUser;
                    userObject.email = $scope.newEmail;
                    User.editUser(userObject).then(function(data){
                        if(data.data.success){
                            app.successMsg = data.data.message;
                            $timeout(function() {
                                app.emailForm.email.$setPristine();
                                app.emailForm.email.$setUntouched();
                                app.successMsg = false;
                                app.disabled = false;
                            }, 2000)
                        } else {
                            $scope.alert = 'alert alert-danger'; // Set class for message
                            app.errorMsg = data.data.message; // Set error message
                            app.disabled = false; // Enable form for editing
                        }
                    });
                } else {
                    $scope.alert = 'alert alert-danger'; // Set class for message
                    app.errorMsg = 'Please ensure form is filled out properly'; // Set error message
                    app.disabled = false; // Enable form for editing
                }
};
// Function: Update the user's username
app.updateUsername = function(newUsername, valid) {
    app.errorMsg = false; // Clear any error message
    app.disabled = true; // Lock form while processing
    // Check if username submitted is valid
    if (valid) {
        var userObject = {}; // Create the user object to pass to function
        userObject._id = app.currentUser; // Pass current user _id in order to edit
        userObject.username = $scope.newUsername; // Set the new username provided
        // Runs function to update the user's username
        User.editUser(userObject).then(function(data) {
            // Check if able to edit user
            if (data.data.success) {
                $scope.alert = 'alert alert-success'; // Set class for message
                app.successMsg = data.data.message; // Set success message
                // Function: After two seconds, clear and re-enable
                $timeout(function() {
                    app.usernameForm.username.$setPristine(); // Reset username form
                    app.usernameForm.username.$setUntouched(); // Reset username form
                    app.successMsg = false; // Clear success message
                    app.disabled = false; // Enable form for editing
                }, 2000);
            } else {
                app.errorMsg = data.data.message; // Set error message
                app.disabled = false; // Enable form for editing
            }
        });
    } else {
        app.errorMsg = 'Please ensure form is filled out properly'; // Set error message
        app.disabled = false; // Enable form for editing
    }
};

// Function: Update the user's permission
app.updatePermissions = function(newPermission) {
    app.errorMsg = false; // Clear any error messages
    app.disableUser = true; // Disable button while processing
    app.disableModerator = true; // Disable button while processing
    app.disableAdmin = true; // Disable button while processing
    var userObject = {}; // Create the user object to pass to function
    userObject._id = app.currentUser; // Get the user _id in order to edit
    userObject.permission = newPermission; // Set the new permission to the user
    // Runs function to udate the user's permission
    User.editUser(userObject).then(function(data) {
        // Check if was able to edit user
        if (data.data.success) {
            $scope.alert = 'alert alert-success'; // Set class for message
            app.successMsg = data.data.message; // Set success message
            // Function: After two seconds, clear and re-enable
            $timeout(function() {
                app.successMsg = false; // Set success message
                $scope.newPermission = newPermission; // Set the current permission variable
                // Check which permission was assigned to the user
                if (newPermission === 'user') {
                    app.disableUser = true; // Lock the 'user' button
                    app.disableModerator = false; // Unlock the 'moderator' button
                    app.disableAdmin = false; // Unlock the 'admin' button
                } else if (newPermission === 'moderator') {
                    app.disableModerator = true; // Lock the 'moderator' button
                    app.disableUser = false; // Unlock the 'user' button
                    app.disableAdmin = false; // Unlock the 'admin' button
                } else if (newPermission === 'admin') {
                    app.disableAdmin = true; // Lock the 'admin' buton
                    app.disableModerator = false; // Unlock the 'moderator' button
                    app.disableUser = false; // unlock the 'user' button
                }
            }, 2000);
        } else {
            $scope.alert = 'alert alert-danger'; // Set class for message
            app.errorMsg = data.data.message; // Set error message
            app.disabled = false; // Enable form for editing
        }
    });
};
});

