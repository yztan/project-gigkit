angular.module('deleteCtrl', [])
.controller('deleteController', function($scope, $http, $routeParams){
    $scope.superhero = {};
    //get the id to query the db and retrieve the correct superhero
    var id = $routeParams.id;
    // $http.get('/superhero/' + id)
    //     .then(function(data){
    //         console.log(JSON.stringify(data));
    //         $scope.superhero = data;
    //     },
    //     function(data) {
    //         console.log('Error: ' + data);
    //     });     

    $http.delete('/api/superhero/delete/'+ id)
    .then(function(data){
        console.log(JSON.stringify(data));
        $scope.superhero = data;
    },
    function(data) {
        console.log('Error: ' + data);
    });     
    
});