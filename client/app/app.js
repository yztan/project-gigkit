angular.module('userApp',['appRoutes', 'usersControllers', 'userServices','ngAnimate', 'mainController','authServices' ,'ngAnimate','ui.bootstrap'
,'ngSanitize','managementController','addSuperheroCtrl','angular-filepicker','galleryCtrl','detailCtrl','deleteCtrl','ngMap','addGigCtrl','deleteGigCtrl','bookCtrl'])

.config(function($httpProvider){
    $httpProvider.interceptors.push('AuthInterceptors')
});