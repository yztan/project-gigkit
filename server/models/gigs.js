// Dependencies
var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

// Defines the superhero schema
var GigSchema = new Schema({
    date: {type: String, required: true},
    city: {type: String, required: true},
    venue: {type: String, required: true},
    duration: {type: String, required: true},
    compensation: {type: String, required: true},
    details: {type: String, required: true},
    contact: {type: String, required: true},
    lon: {type: Number, default: 0, required: true},
    lat: {type: Number, default: 0, required: true},
    // map: {type: String, required: true},
    createdAt: {type: Date, default: Date.now},    
});

// Sets the createdAt parameter equal to the current time
GigSchema.pre('save', function(next){
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});

// Exports the SuperheroSchema for use elsewhere.
module.exports = mongoose.model('gig', GigSchema);
