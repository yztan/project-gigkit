var FacebookStrategy = require("passport-facebook").Strategy;
var User             = require('../models/user');
var session         = require('express-session');
var jwt = require('jsonwebtoken');
var secret = "harrypotter"


module.exports = function(app,passport){

    app.use(passport.initialize());
    app.use(passport.session());
    app.use(session({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: true,
        cookie: { secure: false }
    }));

    passport.serializeUser(function(user, done) {
        token = jwt.sign({ username: user.username, email: user.email}, secret, {expiresIn: '24h'});
        done(null, user.id);
      });
      
      passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
          done(err, user);
        });
      });
    
    passport.use(new FacebookStrategy({
        clientID: "280216659148649",
        clientSecret: "fa4db5e5d0a63329e6c202855ea7a7a2",
        callbackURL: "http://localhost:3000/auth/facebook/callback",
        profileFields: ['id', 'displayName', 'photos', 'email']
      },
      function(accessToken, refreshToken, profile, done) {
        console.log(profile._json.email);
        User.findOne({email: profile._json.email}).select('username password email').exec(function(err,user){
            if (err) done (err);

            if (user && user != null){
                done(null, user);
            } else {            //else there is user on fb but not in our mongo db.So, create new user
                var newUser = new User();
                var uname = profile._json.name.split(" "); //to split FB name and get just first name
             
                newUser.username = uname[0]; //Fb name's 0th index is first name
                newUser.password = accessToken;
                newUser.email = profile._json.email;
                newUser.name = uname;

                newUser.save(function(err){
                 if(err){
                  //console.log(err);
                  done(err);
                 } else{
                  console.log("saving new user...");
                  done(null, newUser);
            }
        });}})}
    ));

    app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/facebookerror' }), function(req, res){
        res.redirect('/facebook/' + token);
    });

    app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

    return passport;

};