var User = require('../models/user');
var Superhero = require('../models/superhero');
var Gig = require('../models/gigs');
var jwt = require('jsonwebtoken');
var secret = 'harrypotter'

module.exports = function(router){
// http:localhost:3000/api/users

router.get('/gig',function(req, res){
    //Query the DB and if no errors, send all the superheroes
    var query = Gig.find({});
    query.exec(function(err, gigs){
        if(err) res.send(err);
        //If no errors, send them back to the client
        res.json(gigs);
    });
});

router.post('/gig',function(req, res){
    //Creates a new superhero
    var newGig = new Gig(req.body);
    //Save it into the DB.
    newGig.save(function(err){
        if(err) res.send(err);
        //If no errors, send it back to the client
        res.json(req.body);
    });
});


router.delete('/gig/delete/:id',function(req, res){
    var id = req.params.id;
    Gig.findByIdAndRemove(req.params.id, function(err) {
        if (err)
            res.send(err);
        else
            res.json({ message: 'Event deleted!'});
    });   
});


router.delete('/superhero/delete/:id',function(req, res){
    var id = req.params.id;
    Superhero.findByIdAndRemove(req.params.id, function(err) {
        if (err)
            res.send(err);
        else
            res.json({ message: 'Picture deleted!'});
    });   
});

router.get('/superhero',function(req, res){
    //Query the DB and if no errors, send all the superheroes
    var query = Superhero.find({});
    query.exec(function(err, superheroes){
        if(err) res.send(err);
        //If no errors, send them back to the client
        res.json(superheroes);
    });
});

router.post('/superhero',function(req, res){
    //Creates a new superhero
    var newSuperhero = new Superhero(req.body);
    //Save it into the DB.
    newSuperhero.save(function(err){
        if(err) res.send(err);
        //If no errors, send it back to the client
        res.json(req.body);
    });
});


router.get('/superhero/:id',function(req, res){
    Superhero.findById(req.params.id, function(err, superhero){
        if(err) res.send(err);
        //If no errors, send it back to the client
        res.json(superhero);
    });     
});



router.post('/users', function(req, res){
    var user = new User(); // Create new User object
    user.username = req.body.username; // Save username from request to User object
    user.password = req.body.password; // Save password from request to User object
    user.email = req.body.email; // Save email from request to User object
    user.name = req.body.name;
         if(req.body.username = null || req.body.username == '' || req.body.password == null || req.body.password == ''|| req.body.email == null| req.body.email == ''|| req.body.name == null| req.body.name == ''){
        res.json({success: false, message:"Ensure username, email, password were provided" })
             }else{
        user.save(function(err){
             if(err){
        res.json({success: false, message: "username already exists!"});
             }else {
        res.json({success: true, message: 'user created!'});
       }
    });
    }
});


router.post('/authenticate', function(req, res) {
    User.findOne({ username: req.body.username }).select('email username password').exec(function(err, user) {
        if (err) throw err;

        if (!user) {
            res.json({ success: false, message: 'Could not authenticate' });
        } else if (user) {
            if (req.body.password) {
                var validPassword = user.comparePassword(req.body.password);
                if (!validPassword) {
                    res.json({ success: false, message: 'Could not validate Password' });
                } else {
                   var token = jwt.sign({ username: user.username, email: user.email}, secret, {expiresIn: '24h'});
                    res.json({ success: true, message: 'User Authenticate', token: token });
                }
            } else {
                res.json({ success: false, message: 'No password provided' });
            }
        }
    });
});

router.use(function(req,res,next){
    var token = req.body.token || req.body.query || req.headers['x-access-token'];

    if(token) {
        jwt.verify(token, secret, function(err, decoded){
            if (err) {
            res.json({success: false, message: "Token invalid"});
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.json({success: false, message:"No token provided"})
    }

});

router.post('/me', function(req, res) {
    res.send(req.decoded);
    
});

router.get('/permission', function(req, res){
    User.findOne({ username: req.decoded.username}, function(err, user){
        if (err) throw err;
        if(!user){
            res.json({ success: false, message: 'No user was found!'})
        } else {
            res.json({ success: true, permission: user.permission });
        }
    });
});

router.get('/management', function(req, res){
    User.find({}, function(err,users){
        if(err) throw err;
        User.findOne({username: req.decoded.username}, function(err, mainUser){
            if(err) throw err;
            if(!mainUser) {
                res.json({success: false, message: 'No user Found'});
            } else {
                if (!mainUser) {
                    res.json({ success: false, message: 'No user found' }); // Return error
                } else {
                    // Check if user has editing/deleting privileges 
                    if (mainUser.permission === 'admin' || mainUser.permission === 'moderator') {
                        // Check if users were retrieved from database
                        if (!users) {
                            res.json({ success: false, message: 'Users not found' }); // Return error
                        } else {
                            res.json({ success: true, users: users, permission: mainUser.permission }); // Return users, along with current user's permission
                        }
                    } else {
                        res.json({ success: false, message: 'Insufficient Permissions' }); // Return access error
            }
        }
        }})
    });
});

router.delete('/management/:username', function(req, res) {
    var deletedUser = req.params.username;
    User.findOne({ username: req.decoded.username }, function(err, mainUser) {
        if(err) throw err;
        if (!mainUser) {
            res.json({ success: false, message: 'No user found' }); // Return error
        } else {
            if (mainUser.permission !== 'admin') {
                res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
            } else {
                User.findOneAndRemove({ username: deletedUser }, function(err, user) {
                    if (err) throw err;
                    res.json({ success: true }); // Return success status
                });
            }
        }
    });
});

router.get('/edit/:id', function(req, res){
    var editUser = req.params.id;
    User.findOne({ username: req.decoded.username}, function(err, mainUser){
        if (err) throw err;
        if(!mainUser) {
            res.json({ success: false, message: 'No user found'});
        } else {
            if (mainUser.permission ==="admin" || mainUser.permission === "moderator") {
                User.findOne({ _id: editUser}, function(err, user){
                    if(err) throw err;
                    if(!user) {
                        res.json({ success: false, message: 'No user found'})
                    } else {
                        res.json({success: true, user: user});
                    }
                });
            } else {
                res.json({ success: false, message: "Insufficient Permission"});
            }
        }
    });
});

router.put('/edit', function(req,res){
    var editUser = req.body._id; // Assign _id from user to be editted to a variable
    if (req.body.name) var newName = req.body.name; // Check if a change to name was requested
    if (req.body.username) var newUsername = req.body.username; // Check if a change to username was requested
    if (req.body.email) var newEmail = req.body.email; // Check if a change to e-mail was requested
    if (req.body.permission) var newPermission = req.body.permission; // Check if a change to permission was requested
    // Look for logged in user in database to check if have appropriate access
    User.findOne({ username: req.decoded.username }, function(err, mainUser) {
        if (err) throw err;
        if (!mainUser) {
            res.json({success: false, message: "No user found"});
        } else {
            if (newName) {
                    if (mainUser.permission === "admin" || mainUser.permission === 'moderator'){
                        User.findOne({ _id: editUser }, function(err, user){
                            if (err) throw err;
                            if (!user) {
                                res.json( {success: false, message: 'No user found'});
                            } else {
                                user.name = newName;
                                user.save(function(err){
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        res.json({ success: true, message: 'Name has been updated'});
                                    }
                                });
                            }
                        });
                    } else {
                        res.json( {success: false, message: 'Insufficient Permissions'});
                    }
                }
                if (newUsername) {
                    if (mainUser.permission === 'admin' || mainUser.permission === 'moderator') {
                        User.findOne({ _id: editUser }, function(err, user) {
                            if (err) throw err;
                            if (!user) {
                                res.json({ success: false, message: 'No user found' }); // Return error
                            } else {
                                user.username = newUsername; // Save new username to user in database
                                // Save changes
                                user.save(function(err) {
                                    if (err) {
                                        console.log(err); // Log error to console
                                    } else {
                                        res.json({ success: true, message: 'Username has been updated' }); // Return success
                                    }
                                });
                            }
                        });
                    } else {
                        res.json( { success: false, message: 'Insufficient permissions'})
                    }
                }
            }
            if (newEmail) {
                if(mainUser.permission === "admin" || mainUser.permission ==="moderator") {
                    User.findOne({ _id: editUser}, function(err, user){
                        if (err) throw err;
                        if (!user) {
                            res.json({ success: false, message: 'No user found' }); // Return error
                        } else {
                            user.email = newEmail; // Assign new e-mail to user in databse
                            // Save changes
                            user.save(function(err) {
                                if (err) {
                                    console.log(err); // Log error to console
                                } else {
                                    res.json({ success: true, message: 'E-mail has been updated' }); // Return success
                                }
                            });
                        }
                    });
                } else {
                    res.json( { success: false, message: 'Insufficient permissions'})
            }
        }
        if (newPermission) {
            if(mainUser.permission === "admin" || mainUser.permission ==="moderator") {
                User.findOne({_id: editUser}, function(err, user){
                    if (err) throw err;
                    if (!user) {
                        res.json({ success: false, message: 'No user found' }); // Return error
                    } else {
                        // Check if attempting to set the 'user' permission
                        if (newPermission === 'user') {
                            // Check the current permission is an admin
                            if (user.permission === 'admin') {
                                // Check if user making changes has access
                                if (mainUser.permission !== 'admin') {
                                    res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to downgrade an admin.' }); // Return error
                                } else {
                                    user.permission = newPermission; // Assign new permission to user
                                    // Save changes
                                    user.save(function(err) {
                                        if (err) {
                                            console.log(err); // Long error to console
                                        } else {
                                            res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                        }
                                    });
                                }
                            } else {
                                user.permission = newPermission; // Assign new permission to user
                                // Save changes
                                user.save(function(err) {
                                    if (err) {
                                        console.log(err); // Log error to console
                                    } else {
                                        res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                    }
                                });
                            }
                        }
                        // Check if attempting to set the 'moderator' permission
                        if (newPermission === 'moderator') {
                            // Check if the current permission is 'admin'
                            if (user.permission === 'admin') {
                                // Check if user making changes has access
                                if (mainUser.permission !== 'admin') {
                                    res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to downgrade another admin' }); // Return error
                                } else {
                                    user.permission = newPermission; // Assign new permission
                                    // Save changes
                                    user.save(function(err) {
                                        if (err) {
                                            console.log(err); // Log error to console
                                        } else {
                                            res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                        }
                                    });
                                }
                            } else {
                                user.permission = newPermission; // Assign new permssion
                                // Save changes
                                user.save(function(err) {
                                    if (err) {
                                        console.log(err); // Log error to console
                                    } else {
                                        res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                    }
                                });
                            }
                        }

                        // Check if assigning the 'admin' permission
                        if (newPermission === 'admin') {
                            // Check if logged in user has access
                            if (mainUser.permission === 'admin') {
                                user.permission = newPermission; // Assign new permission
                                // Save changes
                                user.save(function(err) {
                                    if (err) {
                                        console.log(err); // Log error to console
                                    } else {
                                        res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                    }
                                });
                            } else {
                                res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to upgrade someone to the admin level' }); // Return error
                            }
            }
        }
                });
            } else {
                res.json( { success: false, message: 'Insufficient permissions'})
            }
        }
    })
});

//superheroes

router.get('/superhero/getall',function(req, res){
    //Query the DB and if no errors, send all the superheroes
    var query = Superhero.find({});
    query.exec(function(err, superheroes){
        if(err) res.send(err);
        //If no errors, send them back to the client
        res.json(superheroes);
    });
});


return router;

};